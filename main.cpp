#include <iostream>
#include <string>
#include <pcap.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <set>
#include <utility>

using namespace std;
typedef long long int ll;
typedef long double dl;
typedef pair<dl,dl> pdi;
typedef pair<ll,ll> pii;
typedef pair<ll,pii> piii;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
//cout<<fixed;
//cout.precision(12);

string pat;
int pat_len;

void usage()
{
	cout << "syntax : tcp-block <interface> <pattern>\n";
	cout << "sample : tcp-block wlan1 \"gilgil\"";
}


uint16_t ip_checksum(libnet_ipv4_hdr ip_hdr_p) //with chatgpt
{

	uint16_t ret = 0;
	uint32_t val = 0;
	uint16_t *f_iphdr = (uint16_t *)&ip_hdr_p;
	for (int i = 0; i < 10; i++)
		val += ntohs(split_iphdr[i]);
	
	while (val >> 16)
		val = (val >> 16) + (sum & 0xFFFF);
	
	ret = htons((uint16_t)~val)
	return ret;
}


uint16_t tcp_checksum(TcpPacketHdr *packet, int sz) //with chatgpt
{
	packet->tcp_hdr.th_sum = 0;

	psuedo_hdr psuedo_hdr;
	psuedo_hdr.protocol = packet->ip_hdr.ip_p;
	psuedo_hdr.tcp_len = ntohs(size);
	psuedo_hdr.src_ip = Ip(packet -> ip_hdr.ip_src.s_addr);
	psuedo_hdr.dst_ip = Ip(packet -> ip_hdr.ip_dst.s_addr);
	psuedo_hdr.placeholder = 0;

	uint16_t ret = 0;
	uint32_t val = 0;
	uint16_t *psuedo_hdr_prime = (uint16_t *)&psuedo_hdr;
	for (int i = 0; i < 6; i++)
		val += ntohs(psuedo_hdr_prime[i]);
	
	
	while (val >> 16)
		val = (val >> 16) + (sum & 0xFFFF);

	uint16_t *tcp_prime = (uint16_t *)&packet->tcp_hdr;

	for (int i = 0; i < sz >> 1; i++)
		val += ntohs(tcp_prime[i]);
	
	
	while (val >> 16)
		val = (val >> 16) + (sum & 0xFFFF);

	if (1 & sz)
		val += ntohs(tcp_prime[sz >> 1]);
	
	ret = htons((uint16_t)~val)
	return ret;
}

void blocking(pcap_t *handle, const u_char *packet, int size)
{
    // 1. forward packet (RST Flag)
    // It is okay to discard Payload Data!
    TcpPacketHdr *packet_forward = (TcpPacketHdr *)malloc(sizeof(TcpPacketHdr));
    memcpy(packet_forward, packet, sizeof(TcpPacketHdr));

    packet_forward->ip.ip_len = htons(sizeof(IPv4Hdr) + sizeof(TcpHdr));
    packet_forward->tcp.th_off = 5;
    packet_forward->tcp.th_flags = TH_RST | TH_ACK;

    // Update Checksums
    packet_forward->ip.ip_sum = ip_checksum(packet_forward->ip);
    packet_forward->tcp.th_sum = tcp_checksum(packet_forward, sizeof(TcpHdr));

    // Send Forward Packet
    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(packet_forward), sizeof(TcpPacketHdr));
    if (res != 0)
    {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }

    // 2. backward packet (FIN Flag + redirect data)
    TcpPacketHdr *packet_backward = (TcpPacketHdr *)malloc(sizeof(TcpPacketHdr) + redirect_data.length());
    memcpy(packet_backward, packet, sizeof(TcpPacketHdr));

    // Update Ethernet Header
    packet_backward->eth.dmac_ = packet_backward->eth.smac_;

    // Update IP Header
    packet_backward->ip.ip_len = htons(sizeof(IPv4Hdr) + sizeof(TcpHdr) + redirect_data.length());
    packet_backward->ip.ip_ttl = 128;
    Ip tmp_ip = packet_backward->ip.ip_src;
    packet_backward->ip.ip_src = packet_backward->ip.ip_dst;
    packet_backward->ip.ip_dst = tmp_ip;

    // Update TCP Header
    uint16_t tmp_port = packet_backward->tcp.th_sport;
    packet_backward->tcp.th_sport = packet_backward->tcp.th_dport;
    packet_backward->tcp.th_dport = tmp_port;
    // SYN, ACK
    uint32_t tmp_seq = packet_backward->tcp.th_seq;
    packet_backward->tcp.th_seq = packet_backward->tcp.th_ack;
    // Calculate data payload lenth of original packet (To update ACK)
    uint16_t data_len = ntohs(packet_backward->ip.ip_len) - sizeof(IPv4Hdr) - packet_backward->tcp.th_off * 4;
    packet_backward->tcp.th_ack = tmp_seq + htonl(data_len);
    packet_backward->tcp.th_off = 5;
    packet_backward->tcp.th_flags = TH_FIN | TH_ACK;

    // Add redirect data
    memcpy((char *)packet_backward + sizeof(TcpPacketHdr), redirect_data.c_str(), redirect_data.length());

    // Update Checksums
    packet_backward->ip.ip_sum = ip_checksum(packet_backward->ip);
    packet_backward->tcp.th_sum = tcp_checksum(packet_backward, sizeof(TcpHdr) + redirect_data.length());

    // Send Backward Packet With Raw Socket

    // Create raw socket
    int sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
    if (sockfd < 0)
    {
        std::cerr << "Failed to create socket." << std::endl;
        return;
    }

    // Set socket options to enable IP headers inclusion
    int on = 1;
    if (setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0)
    {
        std::cerr << "Failed to set socket options." << std::endl;
        return;
    }

    // Prepare destination address structure
    struct sockaddr_in destAddr;
    destAddr.sin_family = AF_INET;
    destAddr.sin_port = packet_backward->tcp.th_dport;
    destAddr.sin_addr.s_addr = packet_backward->ip.ip_dst;

    // Create TCP packet
    char rawpacket[4096];
    memset(rawpacket, 0, sizeof(rawpacket));

    // IP header
    struct IPv4Hdr *ipHeader = (struct IPv4Hdr *)rawpacket;
    memcpy(ipHeader, &(packet_backward->ip), sizeof(IPv4Hdr));

    // TCP header
    struct TcpHdr *tcpHeader = (struct TcpHdr *)(rawpacket + sizeof(struct IPv4Hdr));
    memcpy(tcpHeader, &(packet_backward->tcp), sizeof(TcpHdr));

    // TCP payload (HTTP response)
    char *payload = rawpacket + sizeof(struct IPv4Hdr) + sizeof(struct TcpHdr);
    strcpy(payload, redirect_data.c_str());

    // Send the packet
    if (sendto(sockfd, rawpacket, ntohs(ipHeader->ip_len), 0, (struct sockaddr *)&destAddr, sizeof(destAddr)) < 0)
    {
        std::cerr << "Failed to send packet." << std::endl;
        return;
    }

    printf("Redirected To http://warning.or.kr\n");

    // Close socket
    close(sockfd);
    free(packet_forward);
    free(packet_backward);
}

int main(int argc, char *argv[])
{
	if (argc != 3){
		usage();
		return -1;
	}

	char *dev = argv[1];
	string pat = argv[2];

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);

	if (handle == nullptr)
	{
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}

	int pat_len = pat.size();
	
	while (true) {

		struct pcap_pkthdr *header;
		const u_char *packet;

		int pcap_res = pcap_next_ex(handle, &header, &packet);
		if (pcap_res == 0) continue;
		if (res == PCAP_ERROR_BREAK || pcap_res == PCAP_ERROR) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
			break;
		}

		void* eth_hedr = (u_char*)packet;
		void* ip_hedr = (u_char*)packet + sizeof(libnet_ethernet_hdr);
		
		if(((libnet_ipv4_hdr*)ip_hedr)->ip_v != 4 || ((libnet_ipv4_hdr*)ip_hedr)->ip_p != 6)
			continue;
		
		void* tcp_hedr = (u_char*)ip_hedr + ((libnet_ipv4_hdr*)ip_hedr)->ip_hl * 4;
		void* db = (u_char*)tcp_hedr + ((libnet_tcp_hdr*)tcp_header)->th_off * 4; 

		int d_len = ntohs(((libnet_ipv4_hdr*)ip_hedr)->ip_len) - ((libnet_ipv4_hdr*)ip_hedr)->ip_hl * 4 - ((libnet_tcp_hdr*)tcp_hedr)->th_off * 4;
		if(d_len < 0)
		{
			fprintf(stderr, "length error : header size is larger than packet\n");
			continue;
		}

		const u_char *data = packet + sizeof(TcpPacketHdr);
		string s_data = string(data);

		if (s_data.find(pat) != string::npos){
			blocking(handle, packet, header->caplen);
		}


	}


	return 0;
}